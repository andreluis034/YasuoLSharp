﻿using System;
using LeagueSharp.Common;

namespace YasuoTheLastMemebender
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomEvents.Game.OnGameLoad += Game_OnGameLoad;
        }

        private static void Game_OnGameLoad(EventArgs args)
        {
            try
            {
                new YasuoMemeBender();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Failed to load Yasuo - The Memebender: " + exception);
            }
        }
    }
}
